<?php
/**
 * @file
 * commerce_downloads_feature.features.inc
 */

/**
 * Implementation of hook_commerce_product_default_types().
 */
function commerce_downloads_feature_commerce_product_default_types() {
  $items = array(
    'digital_product' => array(
      'type' => 'digital_product',
      'name' => 'Digital Product',
      'description' => 'This product has a commerce file field attached to sell files.',
      'help' => '',
      'module' => 'commerce_product_ui',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function commerce_downloads_feature_node_info() {
  $items = array(
    'digital_product_display' => array(
      'name' => t('Digital Product Display'),
      'base' => 'node_content',
      'description' => t('This content type allows for displaying your digital products through a product reference field. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
